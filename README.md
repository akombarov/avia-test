Для запуска сервиса выполнить команду: 

docker build -t avia-test . && docker run -it -p 8080:8080/tcp avia-test

Проверить можно выполнив GET запрос:

http://localhost:8080/places?term=Москва&locale=ru&types%5B%5D=city&types%5B%5D=airport

Если по каким-либо причинам контейнер не собирается, приложил два файла для запуска сервиса:

Windows: start.bat

Linux: start.sh


**Задание:**

"Реализовать сервис подсказок локации"

У наших друзей aviasales есть замечательный сервис подсказок, который помогает пользователям выбрать город, зная всего пару букв. 
Но, так как у нас много виджетов, и они уже работают совсем с другим форматом, мы хотим завернуть этот сервис через себя, чтобы приводить его в нужный виджетам формат. Вот пример запроса к этому сервису https://places.aviasales.ru/v2/places.json?term=Москва&locale=ru&types%5B%5D=city&types%5B%5D=airport

А вот формат ответа, с которым могут работать наши виджеты

[{"slug":"MOW","subtitle":"Russia","title":"Moscow"},{"slug":"DME","subtitle":"Moscow","title":"Moscow Domodedovo Airport"}, ... ]

Что мы хотим от тебя получить:

Ссылку на репозиторий

Инструкцию как его запустить и проверить

Немного дополнительной информации:

Ребята из aviasales, конечно, молодцы, что сделали такой крутой сервис, но иногда он у них не работает или слишком долго отвечает, для нас это неприемлемо, максимальное время ответа, которое может ждать виджет – 3с. Хорошо, что эти данные нечасто обновляются, и мы сможем кэшировать результаты у себя.

Мы любим docker и docker-compose.

Мы любим хорошие логирование, оно помогает находить проблемы."