package helpers

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

var ServerPort = ":8080"
var UrlAPI string

type AppError struct {
	Error   error
	Message string
	Code    int
}

type ErrHandle func(http.ResponseWriter, *http.Request, httprouter.Params) AppError
