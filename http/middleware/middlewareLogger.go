package middleware

import (
	"fmt"
	"net/http"
	"time"

	"avia-test/helpers"

	"github.com/julienschmidt/httprouter"
)

func (m *Middleware) LogMiddleware(next helpers.ErrHandle) helpers.ErrHandle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) helpers.AppError {
		start := time.Now()
		var appError helpers.AppError
		defer func() {
			m.Logg.Host(r.Host)
			if appError.Error != nil {
				m.Logg.LogErr(
					appError.Error,
					fmt.Sprintf("%s. Params: Url - %s, Method - %s ", appError.Message, r.URL.Path, r.Method))
				http.Error(w, appError.Message, appError.Code)
			} else {
				m.Logg.LogInfo(fmt.Sprintf("Params: Url - %s, Method - %s, Time execution - %v", r.URL.Path, r.Method, time.Since(start)))
			}
		}()
		appError = next(w, r, p)
		return helpers.AppError{Error: nil, Code: 200, Message: ""}
	}
}
