package controller

import (
	"testing"
)

func TestGetPlaces(t *testing.T) {
	_, custErr := sendRequest("https://places.aviasales.ru/v2/places.json?term=%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0&locale=ru&types%5B%5D=city&types%5B%5D=airport")
	if custErr != nil {
		t.Error("GET places.aviasales.ru error. Expected status 200, got ", custErr.Code)
	}
}
