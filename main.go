package main

import (
	"avia-test/helpers"
	"avia-test/route"
	"fmt"
	"log"

	"net/http"
	"os"
)

func main() {
	var exists bool
	logger := helpers.Logg{}
	logger.SetLogPath("./log/")
	logger.Service("avia-test")

	rou := route.Router{Logg: &logger}

	helpers.UrlAPI, exists = os.LookupEnv("URL_API")
	if !exists || helpers.UrlAPI == "" {
		logger.LogErr(fmt.Errorf("error: URL_API not found"), "export variable URL_API")
		log.Fatal("error: URL_API not found")
	}

	logger.LogInfo("service avia-test started work")
	msgPort := "port" + helpers.ServerPort
	logger.LogInfo(msgPort)
	err := http.ListenAndServe(helpers.ServerPort, rou.Route())

	if err != nil {
		logger.LogErr(err, "server finished with error")
	}
	logger.LogInfo(fmt.Sprintf("server finished"))
}
