package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"avia-test/helpers"
	u "net/url"

	"github.com/julienschmidt/httprouter"
)

type RespAviaSails struct {
	Type        string `json:"type"`
	Code        string `json:"code"`
	CityCode    string `json:"city_code"`
	CountryName string `json:"country_name"`
	Name        string `json:"name"`
	CityName    string `json:"city_name"`
}

type RespWidgets struct {
	Slug     string `json:"slug"`
	Subtitle string `json:"subtitle"`
	Title    string `json:"title"`
}

func GetPlaces(w http.ResponseWriter, r *http.Request, ps httprouter.Params) helpers.AppError {
	type errorData struct {
		Code        string `json:"code"`
		Description string `json:"description"`
	}

	url := helpers.UrlAPI + r.URL.RawQuery

	body, custErr := sendRequest(url)

	if custErr != nil {
		return *custErr
	}

	var resp []RespWidgets

	for _, v1 := range *body {
		switch v1.Type {
		case "city":
			r := RespWidgets{
				Slug:     v1.Code,
				Subtitle: v1.CountryName,
				Title:    v1.Name,
			}
			resp = append(resp, r)
		case "airport":
			r := RespWidgets{
				Slug:     v1.Code,
				Subtitle: v1.CityName,
				Title:    v1.Name,
			}
			resp = append(resp, r)
		}
	}

	req, err := json.Marshal(resp)
	if err != nil {
		return helpers.AppError{Error: err, Code: http.StatusInternalServerError, Message: http.StatusText(http.StatusInternalServerError)}
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(req)
	return helpers.AppError{Error: nil, Code: 200, Message: ""}
}

func sendRequest(url string) (*[]RespAviaSails, *helpers.AppError) {
	_, err := u.ParseRequestURI(url)
	if err != nil {
		return nil, &helpers.AppError{Error: err, Code: http.StatusInternalServerError, Message: "invalid url"}
	}

	timeout := time.Duration(3 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}
	resp, err := client.Get(url)
	if err != nil {
		return nil, &helpers.AppError{Error: err, Code: http.StatusInternalServerError, Message: "request canceled while waiting for connection"}
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, &helpers.AppError{Error: err, Code: http.StatusInternalServerError, Message: "send request error"}
	}

	if resp.StatusCode == http.StatusOK {
		var b []RespAviaSails
		err = json.Unmarshal(body, &b)
		if err != nil {
			return nil, &helpers.AppError{Error: err, Code: http.StatusInternalServerError, Message: "unmarshal error"}
		}
		return &b, nil
	}

	return nil, &helpers.AppError{Error: fmt.Errorf(resp.Status), Code: resp.StatusCode, Message: "wrong parameters"}
}
