package middleware

import (
	"fmt"
	"net/http"

	"avia-test/helpers"

	"github.com/julienschmidt/httprouter"
)

type Middleware struct {
	Logg helpers.Logger
}

func (m *Middleware) PanicMiddleware(next helpers.ErrHandle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		defer func() {
			if err := recover(); err != nil {
				err, ok := err.(error)
				if ok {
					m.Logg.LogErr(
						err,
						fmt.Sprintf("%s. Params: Url - %s, Method - %s ", "unexpected panic error", r.URL.Path, r.Method))
				}
				http.Error(w, "Internal server error", 500)
			}
		}()
		next(w, r, p)
	}
}
