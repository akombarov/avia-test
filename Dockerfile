FROM golang:1.11.2
RUN mkdir /app
RUN mkdir /go/src/avia-test
ADD . /go/src/avia-test
WORKDIR /go/src/avia-test
RUN go get ./...
RUN go test ./http/controller/
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app

FROM alpine:latest

RUN apk add --no-cache --update ca-certificates tzdata \
    && cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime \
    && echo "Europe/Moscow" >  /etc/timezone

RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/avia-test/app .
ENV URL_API=https://places.aviasales.ru/v2/places.json?
CMD ["/root/app"]
