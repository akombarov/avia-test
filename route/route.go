package route

import (
	"avia-test/helpers"
	Con "avia-test/http/controller"
	Mid "avia-test/http/middleware"

	"github.com/julienschmidt/httprouter"
)

type Router struct {
	Logg helpers.Logger
}

func (r *Router) Route() *httprouter.Router {
	router := httprouter.New()

	mid := Mid.Middleware{Logg: r.Logg}

	midll := func(next helpers.ErrHandle) httprouter.Handle {
		return mid.PanicMiddleware(mid.LogMiddleware(next))
	}

	router.GET("/places", midll(Con.GetPlaces))

	return router
}
