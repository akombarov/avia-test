package helpers

import (
	"fmt"
	"log"
	"os"
	"time"
)

type Logger interface {
	LogInfo(message string)
	Service(message string)
	Host(message string)
	LogErr(err error, reasonError string)
}

type Logg struct {
	logPath string
}

func (l *Logg) SetLogPath(path string) {
	l.logPath = path
}

func (l *Logg) Service(path string) {
}

func (l *Logg) Host(path string) {
}

// Method for write error
func (l *Logg) LogErr(err error, reasonError string) {
	if err != nil {
		l.logToFile("Error", fmt.Sprintf("%s, Info - %s ", err.Error(), reasonError))
	}
}

// Method for write info
func (l *Logg) LogInfo(message string) {
	l.logToFile("Info", message)
}

// Method writes message to file
func (l *Logg) logToFile(typ, message string) {
	day := time.Now().Format("2006.01.02")
	nameLog := fmt.Sprintf("%s%s.log", l.logPath, day)
	f, err := os.OpenFile(nameLog, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		err = os.MkdirAll(l.logPath, os.ModePerm)
		if err != nil {
			log.Fatalf("error creating dir: %v", err)
		}
		f, err = os.Create(nameLog)
		if err != nil {
			log.Fatalf("error opening file: %v", err)
		}
	}
	defer f.Close()
	logger := log.New(f, fmt.Sprintf("%s %s ", typ, time.Now().Format("2006-01-02 15:04:05")), log.LUTC)
	logger.SetOutput(f)
	logger.Println(message)
	log.Println(message)
}
